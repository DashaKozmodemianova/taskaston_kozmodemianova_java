# TaskASTON_Kozmodemianova_JAVA

Task code 1 - 3 is in the Main class.

Response to task 4:

The bracket sequence: [(((())()(())]] cannot be considered correct because the number of open and closed brackets in the first () and the second [] types are different. For consistency to be considered correct, the following correction options are possible:

1. [[(())()(())]]
2. [((())()(()))]
