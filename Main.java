import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        checkNumber();
        checkName();
        int[] array = getArray();
        checkArray(array);
    }

    static void checkNumber() {
        System.out.println("Введите число : ");
        Scanner scanner = new Scanner(System.in);
        int number = scanner.nextInt();
        if (number > 7) {
            System.out.println("Привет");
        }
    }

    static void checkName() {
        System.out.println("Введите имя : ");
        Scanner scanner = new Scanner(System.in);
        String name = scanner.nextLine();
        if (name.equals("Вячеслав")) {
            System.out.println("Привет, " + name);
        } else {
            System.out.println("Нет такого имени");
        }
    }

    static int[] getArray() {
        System.out.println("Введите длину массива : ");
        Scanner scanner = new Scanner(System.in);
        int size = scanner.nextInt();
        int[] array = new int[size];
        System.out.print("Введите массив целых чисел : " + " ");
        for (int i = 0; i < size; i++) {
            array[i] = scanner.nextInt();
        }
        return array;
    }

    static void checkArray(int[] array) {
        System.out.print("Элементы массива кратные 3 : ");
        for (int i = 0; i < array.length; i++) {
            if ((array[i] % 3 == 0) & (array[i] != 0)) {
                System.out.print(array[i] + " ");
            }
        }
    }
}
